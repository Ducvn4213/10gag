package activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import a10gag.ducvn.a10gag.R;
import model.post;

public class PostDetail extends Activity {

    post _post;

    TextView title, posts_comments;
    Button back;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);

        _post = (post) getIntent().getSerializableExtra("post");

        title = (TextView) findViewById(R.id.title);
        posts_comments = (TextView) findViewById(R.id.points_comments);
        back = (Button) findViewById(R.id.back);
        image = (ImageView) findViewById(R.id.image);

    }

    void configImageView() {

    }

    @Override
    protected void onResume() {
        super.onResume();

        loadData();
    }

    void loadData() {
        Picasso.with(this).load(_post.imageUrl).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                int width = bitmap.getWidth();
                int height = bitmap.getHeight();
                image.setImageBitmap(bitmap);
//                if (height > width) {
//                    image.setScaleType(ImageView.ScaleType.CENTER_CROP);
//                }
//                else {
//                    image.setScaleType(ImageView.ScaleType.MATRIX);
//                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }
}
