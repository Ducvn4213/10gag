package fragment.homeFragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import java.util.List;

import a10gag.ducvn.a10gag.R;
import adapter.PostAdapter;
import fragment.home;
import model.post;
import service.Service;

public class trending extends Fragment {

    ListView content;
    Service service;
    PostAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_content, container, false);

        content = (ListView) view.findViewById(R.id.content);

        service = Service.getInstance();

        setupListView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    void setupListView() {
        content.setOnScrollListener(new AbsListView.OnScrollListener(){

            int mLastFirstVisibleItem;

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem > mLastFirstVisibleItem) {
                    home.getInstance().hideToolbar();
                }
                else if (firstVisibleItem < mLastFirstVisibleItem) {
                    home.getInstance().showToolbar();
                }

                mLastFirstVisibleItem = firstVisibleItem;
            }

            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }
        });
    }

    void loadData() {
        service.getHomePage(Service.HOMEPAGE.TRENDING, new Service.GetPostCallback() {
            @Override
            public void onGetPostSuccess(List<post> data) {
                adapter = new PostAdapter(getActivity(), data);
                content.setAdapter(adapter);
            }

            @Override
            public void onGetPostFail(String error) {

            }
        });
    }
}
