package fragment.homeFragment;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.List;

import a10gag.ducvn.a10gag.R;
import activity.PostDetail;
import adapter.PostAdapter;
import fragment.home;
import model.post;
import service.Service;
import util.utils;

public class hot extends Fragment {

    ListView content;
    Service service;
    PostAdapter adapter;
    List<post> data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_content, container, false);

        content = (ListView) view.findViewById(R.id.content);

        service = Service.getInstance();

        setupListView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }
    void setupListView() {

        content.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getActivity(), PostDetail.class);
                i.putExtra("post", data.get(position));
                startActivity(i);
            }
        });

        content.setOnScrollListener(new AbsListView.OnScrollListener(){

            int mLastFirstVisibleItem;

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem > mLastFirstVisibleItem) {
                    home.getInstance().hideToolbar();
                }
                else if (firstVisibleItem < mLastFirstVisibleItem) {
                    home.getInstance().showToolbar();
                }

                mLastFirstVisibleItem = firstVisibleItem;
            }

            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }
        });
    }

    void loadData() {
        service.getHomePage(Service.HOMEPAGE.HOT, new Service.GetPostCallback() {
            @Override
            public void onGetPostSuccess(List<post> data) {
                hot.this.data = data;
                adapter = new PostAdapter(getActivity(), data);
                content.setAdapter(adapter);
            }

            @Override
            public void onGetPostFail(String error) {

            }
        });
    }
}
