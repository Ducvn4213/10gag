package fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.List;

import a10gag.ducvn.a10gag.R;
import adapter.ViewPagerAdapter;
import fragment.videoFragment.*;
import fragment.videoFragment.home;
import model.tab;
import service.Service;
import util.utils;

public class videos extends Fragment {

    RelativeLayout header;
    LinearLayout toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;

    Service service;

    static videos instance;

    public static videos getInstance() {
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video, container, false);

        header = (RelativeLayout) view.findViewById(R.id.header);
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        toolbar = (LinearLayout) view.findViewById(R.id.toolbar);
        viewPager = (ViewPager) view.findViewById(R.id.pager);

        service = Service.getInstance();

        loadData();

        instance = this;

        return view;
    }

    void loadData() {
        service.getVideoTab(new Service.GetVideoTabCallback() {
            @Override
            public void onGetVideoTabSuccess(List<tab> data) {
                setupViewPager(viewPager, data);
                setupTabLayout();
            }

            @Override
            public void onGetVideoTabFail(String error) {
                //TODO
            }
        });
    }

    void setupViewPager(ViewPager viewPager, List<tab> tabs) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(((AppCompatActivity)getActivity()).getSupportFragmentManager());
        adapter.addFragment(new home(), getString(R.string.home));

        for (tab t : tabs) {
            other o = new other();
            o.setTab(t);
            adapter.addFragment(o, t.name);
        }

        viewPager.setAdapter(adapter);
    }

    void setupTabLayout() {
        tabLayout.setupWithViewPager(viewPager);
    }

    boolean isHideToolbar = false;
    public void hideToolbar() {
        if (isHideToolbar == false) {
            int pxtomove = utils.dpToPx(getActivity(), 100);
            header.animate().translationY(-pxtomove).setDuration(200);
            isHideToolbar = true;
        }
    }

    public void showToolbar() {
        if (isHideToolbar == true) {
            tabLayout.animate().translationY(0).setDuration(200);
            header.animate().translationY(0).setDuration(200);
            isHideToolbar = false;
        }
    }
}
