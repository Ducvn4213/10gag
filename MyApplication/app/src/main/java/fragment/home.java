package fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import a10gag.ducvn.a10gag.R;
import adapter.ViewPagerAdapter;
import fragment.homeFragment.fresh;
import fragment.homeFragment.hot;
import fragment.homeFragment.trending;
import util.utils;

public class home extends Fragment {

    RelativeLayout header;
    LinearLayout toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;

    static home instance;

    public static home getInstance() {
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        header = (RelativeLayout) view.findViewById(R.id.header);
        toolbar = (LinearLayout) view.findViewById(R.id.toolbar);
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        viewPager = (ViewPager) view.findViewById(R.id.pager);

        toolbar.bringToFront();

        setupViewPager(viewPager);
        setupTabLayout();

        instance = home.this;

        return view;
    }

    void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(((AppCompatActivity)getActivity()).getSupportFragmentManager());
        adapter.addFragment(new hot(), getString(R.string.hot));
        adapter.addFragment(new trending(), getString(R.string.trending));
        adapter.addFragment(new fresh(), getString(R.string.fresh));
        viewPager.setAdapter(adapter);
    }

    void setupTabLayout() {
        tabLayout.setupWithViewPager(viewPager);
    }

    boolean isHideToolbar = false;
    public void hideToolbar() {
        if (isHideToolbar == false) {
            int pxtomove = utils.dpToPx(getActivity(), 100);
            header.animate().translationY(-pxtomove).setDuration(200);
            isHideToolbar = true;
        }
    }

    public void showToolbar() {
        if (isHideToolbar == true) {
            header.animate().translationY(0).setDuration(200);
            isHideToolbar = false;
        }
    }
}
