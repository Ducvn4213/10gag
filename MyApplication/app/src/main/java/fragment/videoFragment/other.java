package fragment.videoFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;

import java.util.List;

import a10gag.ducvn.a10gag.R;
import adapter.VideoAdapter;
import fragment.*;
import model.tab;
import model.video;
import service.Service;

public class other extends Fragment {

    tab tab;
    GridView videos;
    Service service;
    VideoAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_other, container, false);

        videos = (GridView) view.findViewById(R.id.videos);

        service = Service.getInstance();

        setupListView();

        return view;
    }

    public void setTab(tab tab) {
        this.tab = tab;
    }

    @Override
    public void onResume() {
        loadData();
        super.onResume();
    }

    void setupListView() {
        videos.setOnScrollListener(new AbsListView.OnScrollListener(){

            int mLastFirstVisibleItem;

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem > mLastFirstVisibleItem) {
                    fragment.videos.getInstance().hideToolbar();
                }
                else if (firstVisibleItem < mLastFirstVisibleItem) {
                    fragment.videos.getInstance().showToolbar();
                }

                mLastFirstVisibleItem = firstVisibleItem;
            }

            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }
        });
    }

    void loadData() {
        service.getVideoOther(this.tab.name, new Service.GetVideoCallback() {
            @Override
            public void onGetVideoSuccess(List<video> data) {
                adapter = new VideoAdapter(getActivity(), data);
                videos.setAdapter(adapter);
            }

            @Override
            public void onGetVideoFail(String error) {

            }
        });
    }
}