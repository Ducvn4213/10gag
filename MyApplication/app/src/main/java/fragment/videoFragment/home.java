package fragment.videoFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.HeaderViewListAdapter;

import java.util.List;

import a10gag.ducvn.a10gag.R;
import adapter.HotVideoAdapter;
import adapter.VideoAdapter;
import model.video;
import service.Service;
import util.HeaderGridView;


public class home extends Fragment {

    View hotParent;
    HeaderGridView videos;
    RecyclerView hot;
    Service service;
    VideoAdapter dataAdapter;
    HotVideoAdapter hotAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_home, container, false);

        hotParent = inflater.inflate(R.layout.view_featured_with_space, container, false);
        videos = (HeaderGridView) view.findViewById(R.id.videos);
        hot = (RecyclerView) hotParent.findViewById(R.id.featured);
        service = Service.getInstance();

        return view;
    }

    @Override
    public void onResume() {
        loadData();
        super.onResume();
    }

    void loadData() {
        service.getVideoHome(new Service.GetVideoHomeCallback() {
            @Override
            public void onGetVideoHomeSuccess(List<video> hot, List<video> data) {
                hotAdapter =  new HotVideoAdapter(getActivity(), hot);
                home.this.hot.setAdapter(hotAdapter);

                dataAdapter = new VideoAdapter(getActivity(), data);
                home.this.videos.addHeaderView(home.this.hotParent);
                home.this.videos.setAdapter(dataAdapter);

            }

            @Override
            public void onGetVideoHomeFail(String error) {

            }
        });
    }
}