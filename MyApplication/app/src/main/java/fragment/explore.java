package fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;

import a10gag.ducvn.a10gag.R;
import adapter.FeaturedAdapter;
import adapter.SectionAdapter;
import model.post;
import model.section;
import service.Service;

public class explore extends Fragment {

    View featuredParent;
    LinearLayout toolbar;
    RecyclerView featured;
    ListView sections;
    Service service;
    FeaturedAdapter featuredAdapter;
    SectionAdapter sectionAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_explore, container, false);

        featuredParent = inflater.inflate(R.layout.view_featured, container, false);

        toolbar = (LinearLayout) view.findViewById(R.id.toolbar);
        featured = (RecyclerView) featuredParent.findViewById(R.id.featured);
        sections = (ListView) view.findViewById(R.id.sections);

        service = Service.getInstance();

        return view;
    }

    @Override
    public void onResume() {
        loadData();
        super.onResume();
    }

    void loadData() {
        service.getExploreFeatured(new Service.GetPostCallback() {
            @Override
            public void onGetPostSuccess(List<post> data) {
                featuredAdapter = new FeaturedAdapter(getActivity(), data);
                featured.setAdapter(featuredAdapter);
            }

            @Override
            public void onGetPostFail(String error) {

            }
        });

        service.getExploreSections(new Service.GetSectionCallback() {
            @Override
            public void onGetSectionSuccess(List<section> data) {
                sectionAdapter = new SectionAdapter(getActivity(), data);
                sections.setAdapter(sectionAdapter);
                sections.addHeaderView(featuredParent);
            }

            @Override
            public void onGetSectionFail(String error) {

            }
        });
    }
}


