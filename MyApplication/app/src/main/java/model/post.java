package model;

import java.io.Serializable;

public class post implements Serializable {
    public enum VOTE {
        NONE,
        UP,
        DOWN
    }

    public String id;
    public String title;
    public String url;
    public String imageUrl;
    public int points;
    public int comments;
    public VOTE vote;

    public String points_comments() {
        return points + " Points  " + comments + " Comments";
    }

    public post() {}
}
