package util;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by ducga on 10/21/2016.
 */

public class utils {

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }
}
