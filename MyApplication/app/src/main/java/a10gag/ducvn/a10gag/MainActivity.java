package a10gag.ducvn.a10gag;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.app.Fragment;

import fragment.explore;
import fragment.home;
import fragment.news;
import fragment.profile;
import fragment.videos;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setNewFragment(new home());
    }

    public void selectFrag(View view) {
        Fragment fr;
        if(view == findViewById(R.id.button_home)) {
            fr = new home();
        }
        else if (view == findViewById(R.id.button_explore)) {
            fr = new explore();
        }
        else if (view == findViewById(R.id.button_videos)) {
            fr = new videos();
        }
        else if (view == findViewById(R.id.button_news)) {
            fr = new news();
        }
        else {
            fr = new profile();
        }

        setNewFragment(fr);
    }

    void setNewFragment(Fragment fr) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_place, fr);
        fragmentTransaction.commit();
    }

}
