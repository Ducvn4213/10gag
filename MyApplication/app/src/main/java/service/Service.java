package service;


import java.util.ArrayList;
import java.util.List;

import model.post;
import model.section;
import model.tab;
import model.video;

public class Service {
    private Service() {};
    private static Service instance;

    public static Service getInstance() {
        if (instance == null) {
            instance = new Service();
        }

        return instance;
    }



    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //FOR VIDEO
    public interface GetVideoTabCallback {
        public void onGetVideoTabSuccess(List<tab> data);
        public void onGetVideoTabFail(String error);
    }
    public void getVideoTab(final GetVideoTabCallback callback) {
        List<tab> results = new ArrayList<>();

        tab t = new tab("WOW");
        results.add(t);

        t = new tab("LOL");
        results.add(t);

        t = new tab("MOVIE & TV");
        results.add(t);

        t = new tab("FAIL");
        results.add(t);

        t = new tab("WTF");
        results.add(t);

        t = new tab("SPORT");
        results.add(t);

        t = new tab("HALLOWEEN");
        results.add(t);

        t = new tab("GAMING");
        results.add(t);

        t = new tab("AWW");
        results.add(t);

        callback.onGetVideoTabSuccess(results);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //FOR HOME
    public interface GetPostCallback {
        public void onGetPostSuccess(List<post> data);
        public void onGetPostFail(String error);
    }
    public enum HOMEPAGE {
        HOT,
        TRENDING,
        FRESH
    }
    public void getHomePage(HOMEPAGE page, final GetPostCallback callback) {
        List<post> results = new ArrayList<>();

        results.add(null);

        post p = new post();
        p.title = "post 1";
        p.imageUrl = "http://img-9gag-fun.9cache.com/photo/avrp8GX_700b.jpg";
        p.points = 5000;
        p.comments = 4213;
        p.url = "http://9gag.com/gag/avrp8GX";
        p.vote = post.VOTE.DOWN;
        results.add(p);

        p = new post();
        p.title = "post 2";
        p.imageUrl = "http://img-9gag-fun.9cache.com/photo/a4ZKNEZ_460s_v1.jpg";
        p.points = 5000;
        p.comments = 4213;
        p.url = "http://9gag.com/gag/avrp8GX";
        p.vote = post.VOTE.DOWN;
        results.add(p);

        p = new post();
        p.title = "post 3";
        p.imageUrl = "http://img-9gag-fun.9cache.com/photo/anyqL4q_460s_v1.jpg";
        p.points = 5000;
        p.comments = 4213;
        p.url = "http://9gag.com/gag/avrp8GX";
        p.vote = post.VOTE.DOWN;
        results.add(p);

        p = new post();
        p.title = "post 4";
        p.imageUrl = "http://img-9gag-fun.9cache.com/photo/anyq2mL_460s.jpg";
        p.points = 5000;
        p.comments = 4213;
        p.url = "http://9gag.com/gag/avrp8GX";
        p.vote = post.VOTE.DOWN;
        results.add(p);

        callback.onGetPostSuccess(results);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //FOR EXPLORE
    public void getExploreFeatured(final GetPostCallback callback) {
        List<post> results = new ArrayList<>();

        post p = new post();
        p.title = "post 1";
        p.imageUrl = "http://img-9gag-fun.9cache.com/photo/aYKN0G7_460s.jpg";
        p.points = 5000;
        p.comments = 4213;
        p.url = "http://9gag.com/gag/avrp8GX";
        p.vote = post.VOTE.DOWN;
        results.add(p);

        p = new post();
        p.title = "post 2";
        p.imageUrl = "http://img-9gag-fun.9cache.com/photo/a2dNqVO_460s.jpg";
        p.points = 5000;
        p.comments = 4213;
        p.url = "http://9gag.com/gag/avrp8GX";
        p.vote = post.VOTE.DOWN;
        results.add(p);

        p = new post();
        p.title = "post 3";
        p.imageUrl = "http://img-9gag-fun.9cache.com/photo/avr02Q5_460s.jpg";
        p.points = 5000;
        p.comments = 4213;
        p.url = "http://9gag.com/gag/avrp8GX";
        p.vote = post.VOTE.DOWN;
        results.add(p);

        p = new post();
        p.title = "post 4";
        p.imageUrl = "http://img-9gag-fun.9cache.com/photo/a5ZeXNL_460s.jpg";
        p.points = 5000;
        p.comments = 4213;
        p.url = "http://9gag.com/gag/avrp8GX";
        p.vote = post.VOTE.DOWN;
        results.add(p);

        callback.onGetPostSuccess(results);
    }

    public interface GetSectionCallback {
        public void onGetSectionSuccess(List<section> data);
        public void onGetSectionFail(String error);
    }
    public void getExploreSections(final GetSectionCallback callback) {
        List<section> results = new ArrayList<>();
        List<post> section_post = new ArrayList<>();

        post p = new post();
        p.imageUrl = "http://img-9gag-fun.9cache.com/photo/a8pLZd3_460s.jpg";
        section_post.add(p);

        p = new post();
        p.imageUrl = "http://img-9gag-fun.9cache.com/photo/amrNZD6_460s.jpg";
        section_post.add(p);

        p = new post();
        p.imageUrl = "http://img-9gag-fun.9cache.com/photo/arOvB4K_460s_v1.jpg";
        section_post.add(p);

        p = new post();
        p.imageUrl = "http://img-9gag-fun.9cache.com/photo/a5ZdN2O_460s.jpg";
        section_post.add(p);

        section s = new section();
        s.title = "section 1";
        s.posts = section_post;
        results.add(s);

        s = new section();
        s.title = "section 2";
        s.posts = section_post;
        results.add(s);

        s = new section();
        s.title = "section 3";
        s.posts = section_post;
        results.add(s);

        s = new section();
        s.title = "section 4";
        s.posts = section_post;
        results.add(s);

        callback.onGetSectionSuccess(results);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //FOR VIDEO
    public interface GetVideoCallback {
        public void onGetVideoSuccess(List<video> data);
        public void onGetVideoFail(String error);
    }
    public void getVideoOther(String tab_id, final GetVideoCallback callback) {
        List<video> results = new ArrayList<>();

        results.add(null);
        results.add(null);

        video v = new video();
        v.title = "title 1";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/aW8QR72_460s.jpg";
        results.add(v);

        v = new video();
        v.title = "title 2";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/a37PZev_460s.jpg";
        results.add(v);

        v = new video();
        v.title = "title 3";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/aBwRQjO_460s.jpg";
        results.add(v);

        v = new video();
        v.title = "title 4";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/aW8QR72_460s.jpg";
        results.add(v);

        v = new video();
        v.title = "title 5";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/aXA0Qyv_460s.jpg";
        results.add(v);

        v = new video();
        v.title = "title 6";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/a37PZev_460s.jpg";
        results.add(v);

        v = new video();
        v.title = "title 7";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/aVDzARK_460s.jpg";
        results.add(v);

        v = new video();
        v.title = "title 8";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/avrp8GX_700b.jpg";
        results.add(v);

        v = new video();
        v.title = "title 5";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/aXA0Qyv_460s.jpg";
        results.add(v);

        v = new video();
        v.title = "title 6";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/a37PZev_460s.jpg";
        results.add(v);

        v = new video();
        v.title = "title 7";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/aVDzARK_460s.jpg";
        results.add(v);

        v = new video();
        v.title = "title 8";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/avrp8GX_700b.jpg";
        results.add(v);

        callback.onGetVideoSuccess(results);
    }

    public interface GetVideoHomeCallback {
        public void onGetVideoHomeSuccess(List<video> hot, List<video> data);
        public void onGetVideoHomeFail(String error);
    }
    public void getVideoHome(final GetVideoHomeCallback callback) {
        List<video> results_hot = new ArrayList<>();
        List<video> results_data = new ArrayList<>();

        video v = new video();
        v.title = "title 1";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/aW8QR72_460s.jpg";
        results_hot.add(v);

        v = new video();
        v.title = "title 2";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/a37PZev_460s.jpg";
        results_hot.add(v);

        v = new video();
        v.title = "title 3";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/avrp8GX_700b.jpg";
        results_hot.add(v);

        v = new video();
        v.title = "title 4";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/avrp8GX_700b.jpg";
        results_data.add(v);

        v = new video();
        v.title = "title 5";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/avrp8GX_700b.jpg";
        results_data.add(v);

        v = new video();
        v.title = "title 6";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/avrp8GX_700b.jpg";
        results_data.add(v);

        v = new video();
        v.title = "title 7";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/avrp8GX_700b.jpg";
        results_data.add(v);

        v = new video();
        v.title = "title 8";
        v.imageUrl = "http://img-9gag-fun.9cache.com/photo/avrp8GX_700b.jpg";
        results_data.add(v);

        callback.onGetVideoHomeSuccess(results_hot, results_data);
    }
}
