package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import a10gag.ducvn.a10gag.R;
import model.video;

public class HotVideoAdapter extends RecyclerView.Adapter<HotVideoAdapter.Holder> {

    List<video> data;
    Context context;

    public class Holder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView title, posts_comments;

        public Holder(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            posts_comments = (TextView) view.findViewById(R.id.points_comments);
        }
    }


    public HotVideoAdapter(Context context, List<video> moviesList) {
        this.data = moviesList;
        this.context = context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_hot_video, parent, false);

        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        video v = data.get(position);
        holder.title.setText(v.title);
        Picasso.with(context).load(v.imageUrl).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}