package adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import a10gag.ducvn.a10gag.R;
import model.post;
import model.section;

public class SectionAdapter extends BaseAdapter {
    List<section> data;
    Context context;
    private static LayoutInflater inflater = null;

    public SectionAdapter(Activity activity, List<section> data) {
        this.data = data;
        context = activity;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        ImageView image1, image2, image3, image4;
        TextView title;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder;

        if (convertView == null || convertView.getTag() == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.item_section, parent, false);

            holder = new Holder();
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.image1 = (ImageView) convertView.findViewById(R.id.image1);
            holder.image2 = (ImageView) convertView.findViewById(R.id.image2);
            holder.image3 = (ImageView) convertView.findViewById(R.id.image3);
            holder.image4 = (ImageView) convertView.findViewById(R.id.image4);

            convertView.setTag(holder);
        }
        else {
            holder = (Holder) convertView.getTag();
        }

        section s = data.get(position);
        holder.title.setText(s.title);
        Picasso.with(context).load(s.posts.get(0).imageUrl).into(holder.image1);
        Picasso.with(context).load(s.posts.get(1).imageUrl).into(holder.image2);
        Picasso.with(context).load(s.posts.get(2).imageUrl).into(holder.image3);
        Picasso.with(context).load(s.posts.get(3).imageUrl).into(holder.image4);
        return convertView;
    }

}