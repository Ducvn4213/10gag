package adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import a10gag.ducvn.a10gag.R;
import model.post;
import model.video;

public class VideoAdapter extends BaseAdapter {
    List<video> data;
    Context context;
    private static LayoutInflater inflater = null;

    public VideoAdapter(Activity activity, List<video> data) {
        this.data = data;
        context = activity;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        ImageView image;
        TextView title;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (position < 2 && data.get(position) == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            View view = inflater.inflate(R.layout.item_empty, parent, false);
            return view;
        }

        Holder holder;

        if (convertView == null || convertView.getTag() == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.item_video, parent, false);

            holder = new Holder();
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.image = (ImageView) convertView.findViewById(R.id.image);

            convertView.setTag(holder);
        }
        else {
            holder = (Holder) convertView.getTag();
        }

        video v = data.get(position);
        holder.title.setText(v.title);
        Picasso.with(context).load(v.imageUrl).into(holder.image);

        return convertView;
    }

}