package adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import a10gag.ducvn.a10gag.MainActivity;
import a10gag.ducvn.a10gag.R;
import model.post;

public class PostAdapter extends BaseAdapter {
    List<post> data;
    Context context;
    private static LayoutInflater inflater = null;

    public PostAdapter(Activity activity, List<post> data) {
        this.data = data;
        context = activity;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        ImageView image;
        TextView title, points_comments;
        Button upvote, downvote, comment, more, share;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (position == 0) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            View view = inflater.inflate(R.layout.item_empty, parent, false);
            return view;
        }

        Holder holder;

        if (convertView == null || convertView.getTag() == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.item_post, parent, false);

            holder = new Holder();
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.points_comments = (TextView) convertView.findViewById(R.id.points_comments);
            holder.image = (ImageView) convertView.findViewById(R.id.image);
            holder.upvote = (Button) convertView.findViewById(R.id.upvote);
            holder.downvote = (Button) convertView.findViewById(R.id.downvote);
            holder.more = (Button) convertView.findViewById(R.id.more);
            holder.share = (Button) convertView.findViewById(R.id.share);

            convertView.setTag(holder);
        }
        else {
            holder = (Holder) convertView.getTag();
        }

        post p = data.get(position);
        holder.title.setText(p.title);
        holder.points_comments.setText(p.points_comments());
        Picasso.with(context).load(p.imageUrl).into(holder.image);

        return convertView;
    }

}